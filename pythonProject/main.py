from flask import Flask, render_template, request
import threading
from time import sleep
import ebay_parser
import csv


app = Flask(__name__)

greeting_responses = ['привет', 'здравствуйте', 'здравствуй', 'здарова', 'hi', 'hello', 'hey', 'howdy', 'ку']

# хранит купленный товар
item_sold = None

# Входная функция
@app.route("/")
def index():

    # загрузка данных
    data = data_pull()

    # разбиение программы на разные потоки
    threading.Thread(target=prices_check).start()

    # вывод web страницы
    return render_template("index.html", name=[data[0][0]['name']], wallet=[data[1][0]['balance']])


# обработка GET запроса
@app.route("/get")
def get_response():

    global item_sold

    # для отслеживания проданного товара
    if item_sold != None:

        print(item_sold, 'response')

        data = data_pull()
        balance = int(data[1][0]['balance']) - int(item_sold[1])

        data_del(data)
        balance_update(balance)

        # добавление задержки на удаление item_sold
        threading.Thread(target=delay).start()
        return 'Товар  "' + item_sold[0] + '"' + ' был куплен за ' + str(int(item_sold[1])) + ' руб.'

    # получаем сообщение из input
    user_message = request.args.get("msg")
    print(user_message)

    # Основная логика бота
    try:
        if user_message == '':
            return 'Извините, но я не знаю такой команды :( Попробуйте поздороваться!'

        elif greeting_check(user_message):
            return 'У меня есть следующие команды: добавить товар, удалить товар, изменить баланс, изменить имя'

        elif user_message.lower().strip() == 'изменить имя':

            return 'Используйте следующую команду: /name <имя>'

        elif user_message.lower().strip() == "добавить товар":
            return 'Используйте следующую команду: /add <ссылка товара на eBay> <пороговая-цена(пример="1000")>'

        elif user_message.lower().strip() == "изменить баланс":
            return 'Используйте следующую команду: /balance <количетсво средств(пример="10000")>'

        elif user_message.lower().strip() == "удалить товар":
            return 'Используйте следующую команду: /clear'

        elif ("/name " or '/name') in user_message:
            if user_message.strip() == "/name":
                return 'Имя не введено'
            elif '/name ' in user_message.strip() and len(user_message) > 6:

                new_name = user_message.replace('/name ', '')
                # data_push(data)
                data = data_pull()

                # изменяем имя в базе данных
                data[0][0]['name'] = new_name
                data[1][0]['name'] = new_name

                data_push(data)

                return 'Имя установлено!'

        elif ('/add' or '/add ') in user_message:
            if user_message.strip() == "/add":
                return 'Неверная команда'
            elif '/add ' in user_message.strip() and len(user_message.split()) == 3:

                user_message = user_message.split()
                data = data_pull()

                print(type(user_message[2]))

                if len(data[0]) == 1:
                    if user_message[2].isnumeric():
                        print('ins')
                        try:
                            print('error')
                            url_data = ebay_parser.get_data(user_message[1])
                            item_values = ebay_parser.parse(url_data)
                            data[0][0]['item'] = item_values[0]
                            data[0][0]['item_price'] = item_values[1]
                            data[0][0]['bar_price'] = user_message[2]

                            data_push(data)

                            return 'Товар "' + item_values[0] + '"' + ' с ценой - ' + item_values[1] + ' ' + 'был добавлен'
                        except UnboundLocalError:
                            return 'Неправильный url-адрес! Введите ссылку страницы товара на eBay'
                    else:
                        return "Неверное пороговое число"

                else:
                    if user_message[2].isnumeric():

                        try:

                            url_data = ebay_parser.get_data(user_message[1])
                            item_values = ebay_parser.parse(url_data)

                            data[0][0] = item_values[0]
                            data[0][0]['item_price'] = item_values[1]
                            data[0][0]['bar_price'] = user_message[2]

                            data_push(data)

                            return 'Товар - ' + item_values[0] + ' с ценой - ' + item_values[1] + ' ' + 'был добавлен'
                        except UnboundLocalError:
                            return 'Неправильный url-адрес! Введите ссылку страницы товара на eBay'
                    else:
                        return "Неверное пороговое число"

        elif ('/balance' or '/balance ') in user_message:

            balance = user_message.replace('/balance ', '')
            if balance.isnumeric():

                data = data_pull()

                data[1][0]['balance'] = balance
                data_push(data)

                return balance + 'b'
            else:
                return 'Средства были введены некорректно'

        elif user_message.lower().strip() == '/clear':

            data = data_pull()

            data_del(data)

            return 'Товар удален'


        raise ValueError


    except (ValueError, TypeError):
        return 'Извините, но я не знаю такой команды :( Попробуйте поздороваться!'


# Функция загрузки данных
def data_pull():
    data = []
    wallet = []
    with open("data.csv", encoding='UTF-8', newline='') as file:
        reader = csv.DictReader(file, delimiter=";")
        for row in reader:
            data.append(row)

    with open("wallet.csv", encoding='UTF-8', newline='') as file:
        reader = csv.DictReader(file, delimiter=";")
        for row in reader:
            wallet.append(row)

    return [data, wallet]


# Функция удаления данных из data.csv
def data_del(data):
    with open('data.csv', 'w', encoding='UTF-8', newline='') as file:
        writer = csv.writer(file, delimiter=";")
        writer.writerow(['name', 'item', 'item_price', 'bar_price'])

        writer.writerow([data[0][0]['name'], 'none', 'none', 'none'])

# Загрузка данных в data.csv и wallet.csv
def data_push(data):
    with open('data.csv', 'w', encoding='UTF-8', newline='') as file:
        writer = csv.writer(file, delimiter=";")
        writer.writerow(['name', 'item', 'item_price', 'bar_price'])

        writer.writerow([data[0][0]['name'], data[0][0]['item'], data[0][0]['item_price'], data[0][0]['bar_price']])


    with open('wallet.csv', 'w', encoding='UTF-8', newline='') as file:
        writer = csv.writer(file, delimiter=";")
        writer.writerow(['balance'])

        writer.writerow([data[1][0]['balance']])


# проверка приветствий пользователя
def greeting_check(msg):
    for i in greeting_responses:
        if i in msg.lower().strip():
            return True

    return False

# задержка на стирание переменной item_sold
def delay():

    global item_sold
    sleep(0.2)
    item_sold = None


# обновление баланса
def balance_update(balance):
    with open('wallet.csv', 'w', encoding='UTF-8', newline='') as file:
        writer = csv.writer(file, delimiter=";")
        writer.writerow(['balance'])

        writer.writerow([balance])


# проверка цен (происходит покупка, если цена стала ниже порогового значения)
def prices_check():

    global item_sold

    while True:

        data = data_pull()


        for item in data[0]:

            if data[0][0]['item'] != 'none' and item_sold is None:
            # переформатируем str в int
                item['item_price'] = item['item_price'].replace(u'\xa0', "")
                item['item_price'] = item['item_price'].replace(' руб.', '')
                item['item_price'] = item['item_price'].replace(",", '.')

                item['item_price'] = float(item['item_price'])
                item['bar_price'] = float(item['bar_price'])

                if (item['item_price'] <= item['bar_price']) and (int(data[1][0]['balance']) >= item['item_price']):

                    item_sold = [item['item'], item['item_price']]

        sleep(3)

if __name__ == "__main__":

    # Входная точка
    app.run(debug=True)