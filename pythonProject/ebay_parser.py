from bs4 import BeautifulSoup
import requests

# получаем html страницу товара на eBay
def get_data(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(url)
    return soup


# Находим на странице название и цену товара
def parse(soup):
    title_results = soup.find_all('h1', {'class': 'it-ttl'})
    price_results = soup.find_all('div', {'class': 'convPrice'})

    for item in price_results:
        price = item.find('span').text
        price = price.replace('(включая доставку)', '')

    for item in title_results:

        if item.span:
            _ = item.span.extract()

        if item.a:
            _ = item.a.extract()

        title = item.text

    title = title.strip('\n')
    title = title.strip('-')

    print('parsed successfully')
    return ([title, price])

